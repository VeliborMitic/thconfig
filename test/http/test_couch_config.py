import random
import pytest

from thconfig.http import CouchConfig
from thconfig.error import CouchConfigError


@pytest.mark.asyncio
async def test_couch_config():
    '''
    This function tests couch config initialization, expect success
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config'

    # config
    config = CouchConfig(URI)
    assert config._uri == URI


@pytest.mark.asyncio
async def test_couch_config_error():
    '''
    This function tests couch config error
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config'

    try:
        # zero divison error
        async with CouchConfig(URI, commit=True) as config:
            1 / 0
    except Exception as e:
        pass


@pytest.mark.asyncio
async def test_couch_config_set_get():
    '''
    This function tests couch set and get, expect success
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_set_get'

    # set
    async with CouchConfig(URI, commit=True) as config:
        title = 'Couch Config Example'
        config.title = title
        
        database = {'server': '192.168.1.1'}
        config['database'] = database

    # get
    async with CouchConfig(URI, fetch=True) as config:
        title = config['title']
        assert title == 'Couch Config Example'

        title = config.title
        assert title == 'Couch Config Example'

        database = config['database']
        assert database['server'] == '192.168.1.1'


@pytest.mark.asyncio
async def test_couch_config_get_attr():
    '''
    This function tests get attribute, expect success
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_get_attr'
    
    async with CouchConfig(URI) as config:
        title = 'Couch Config Example'
        database = {'server': '192.168.1.1'}

        config['title'] = title
        config['database'] = database
                
        # get attribute
        get_title_attr = config.title
        assert get_title_attr == title


@pytest.mark.asyncio
async def test_couch_config_get_aexit():
    '''
    This function tests aenter and aexit, on shutdown
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_get_aexit'
    
    config = CouchConfig(URI, commit=True)
    await config.__aenter__()
    
    # aexit
    await config.__aexit__(None, None, None)


@pytest.mark.asyncio
async def test_couch_config_commit():
    '''
    This function tests commit data to couchdb, expect success
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit'
    config = CouchConfig(URI)

    # set
    title = 'Couch Config Example'
    config['title'] = title

    database = {'server': '192.168.1.1'}
    config['database'] = database

    # commit
    commit_ = (await config.commit()).unwrap()
    assert commit_
    
    
@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_couch_config_commit_error():
    '''
    This function tests commit data to couchdb, expect ValueError
    '''    
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit_error'

    config = CouchConfig(uri = f'test, {URI}')
    (await config.commit()).unwrap()


@pytest.mark.asyncio
async def test_couch_config_commit_fetch():
    '''
    This function tests fetch data from couchdb, expect success
    '''        
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit_fetch'
    config = CouchConfig(URI)

    # set
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    config['title'] = title
    config['database'] = database
            
    # commit
    await config.commit()

    # fetch
    fetch_ = (await config.fetch()).unwrap()
    assert fetch_
    assert config['title'] == title
    assert config['database'] == database


@pytest.mark.asyncio
async def test_couch_config_commit_changes():
    '''
    This function tests commit data to couch db, change some data on config object and commit again, expect success
    '''
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit_changes'
    config = CouchConfig(URI)

    # set
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    config['title'] = title
    config['database'] = database

    # commit
    commit_0 = (await config.commit()).unwrap()
    assert commit_0

    # set changes
    changed_title = 'Changed title'
    config['title'] = changed_title

    # commit changes
    commit_1 = (await config.commit()).unwrap()
    assert commit_1

    # fetch
    fetch_ = (await config.fetch()).unwrap()
    assert fetch_
    assert config['title'] == changed_title
    assert config['database'] == database

    
@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_couch_config_fetch_error_invalid_URI():
    '''
    This function tests fetch data from invalid URI, expected ValueError
    '''        
    config = CouchConfig('URI')
    (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=CouchConfigError, strict=True)
async def test_couch_config_fetch_error_bad_request_invalid_db_name():
    '''
    This function tests fetch, when invalid db name in URI, expected CouchConfigError
    '''
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/Thconfig-test/test_couch_config_fetch_error_bad_request_invalid_db_name'
    config = CouchConfig(URI)
    (await config.fetch()).unwrap()


@pytest.mark.asyncio
async def test_couch_config_commit_fetch_config_from_non_existing_doc():
    '''
    This function tests attempt to fetch from non-existing couch document
    '''
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_fetch_non_existing_doc'
    config = CouchConfig(URI)

    # fetch
    fetch_ = (await config.fetch()).unwrap()
    assert fetch_
    assert not config._state


@pytest.mark.asyncio
@pytest.mark.xfail(raises=CouchConfigError, strict=True)
async def test_couch_config_commit_error_bad_request_invalid_db_name():
    '''
    This function tests commit when invalid db name in URI, expects CouchConfigError
    '''
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/Thconfig-test/test_couch_config_commit_changes'
    config = CouchConfig(URI)

    # set
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    config['title'] = title
    config['database'] = database

    # commit
    commit_0 = (await config.commit()).unwrap()
    assert commit_0


@pytest.mark.asyncio
async def test_couch_config_commit_to_non_existing_doc():
    '''
    This function tests commit to non-existing couch document, creates a new document
    '''
    doc_id: int = random.randint(0, 0xffffffff)
    URI = f'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/{doc_id}'
    config = CouchConfig(URI)

    # set
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    config['title'] = title
    config['database'] = database

    # commit
    commit_0 = (await config.commit()).unwrap()
    assert commit_0
    assert config['title'] == title
    assert config['database'] == database
    
