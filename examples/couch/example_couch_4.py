from thconfig.http import CouchConfig


async def example_4():
    '''
    Instantiate CouchConfig,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
    
    # uri for couchdb where are configuration data 
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_fetch_non_existing_doc'

    # create intance CouchConfig and set URI property
    config = CouchConfig(URI)
  
    # fetch
    fetch_ = (await config.fetch()).unwrap()
