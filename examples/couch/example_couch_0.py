from thconfig.http import CouchConfig


'''
Instantiate CouchConfig

parameters:
    uri: str,
    fetch: bool = True,
    commit: bool = False
'''

# uri for couchdb where are configuration data 
URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config'

# create intance CouchConfig and set URI property
config = CouchConfig(URI)
