from thconfig.http import CouchConfig


## error handling fetch
async def example_6():
    '''
    A standard wat to handling exceptions
    with try and except block
    
    Instantiate CouchConfig,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
        
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')

    # standard way to handle exception using try and except blocks
    try:
        (await config.fetch()).unwrap()
    except:
        pass
    