from thconfig.http import CouchConfig


## error handling fetch
async def example_7():
    '''
    Handling exception using unwrap_value function
    that returns exception's message as string and
    doesen't raise exception
    
    Instantiate CouchConfig,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
    
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')
 
    # fetch and unwraps_value - returns exception's message as string
    (await config.fetch()).unwrap_value()
      