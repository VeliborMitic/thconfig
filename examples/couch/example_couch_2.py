from thconfig.http import CouchConfig


async def example_2():
    '''
    Instantiate CouchConfig,
    Commit changes,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    Commit changes:
        parameters:
            self: CouchConfig
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
    
    # uri for couchdb where are configuration data 
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit_fetch'

    # create intance CouchConfig and set URI property
    config = CouchConfig(URI)

    # set title
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    # set database
    config['title'] = title
    config['database'] = database
            
    # commit
    (await config.commit()).unwrap()

    # fetch
    fetch_ = (await config.fetch()).unwrap()
