from thconfig.http import CouchConfig


async def example_1():
    '''
    Instantiate CouchConfig,
    Commit changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
    
    Commit changes:
        parameters:
            self: CouchConfig
    '''
    
    # uri for couchdb where are configuration data 
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_set_get'

    # set config params for couchdb 
    async with CouchConfig(URI, commit = True) as config:
        
        # set title
        title = 'Couch Config Example'
        config.title = title

        # set database
        database = {'server': '192.168.1.1'}
        config['database'] = database

        # commit config params to couchdb
        (await config.commit()).unwrap()
