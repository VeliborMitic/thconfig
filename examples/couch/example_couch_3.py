from thconfig.http import CouchConfig


async def example_3():
    '''
    Instantiate CouchConfig,
    Commit changes,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False

    Commit changes:
        parameters:
            self: CouchConfig
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
    
    # uri for couchdb where are configuration data 
    URI = 'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/test_couch_config_commit_changes'

    # create intance CouchConfig and set URI property
    config = CouchConfig(URI)

    # set title
    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    # set database
    config['title'] = title
    config['database'] = database

    # commit
    commit_0 = (await config.commit()).unwrap()
    assert commit_0

    # change some configuration data
    changed_title = 'Changed title'
    config['title'] = changed_title

    # commit changes to couchdb
    commit_1 = (await config.commit()).unwrap()
    assert commit_1

    # fetch
    fetch_ = (await config.fetch()).unwrap()
