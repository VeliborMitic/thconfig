from thconfig.file import FileConfig

'''
FileConfig - a class to handle reading and writing
configuration data from file
    
Instantiate FileConfig:
    parameters:
        path: str,
        fetch: bool = True,
        commit: bool = False
'''

# you need to provide file with data { configuration }
config_path = 'example_1.json'
    
# create instance of FileConfig
config = FileConfig(config_path)
