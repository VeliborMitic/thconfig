from thconfig.http import EtcdConfig


## Error Handling commit
async def example_8():
    '''
    Handling exception using unwrap_or function
    that returns custom_message as string and
    doesen't raise exception
    
    Instantiate EtcdConfig,
    Commit changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False

    commit changes:
        parameters:
            self: EtcdConfig
            
    unwrap_or:
        parameters:
            custom_message: str
    '''
    
    # You need to provide host and port
    HOST = 'wrong-host'
    PORT = 2379

    # Create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)
 
    # commit and unwraps_or - returns custom message as string
    (await config.commit()).unwrap_or('Handling error with custom message') # Handling error with custom message
      