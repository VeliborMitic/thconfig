from thconfig.http import EtcdConfig


async def example_4():
    '''
    Instantiate EtcdConfig,
    Fetch changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False
            
    Fetch changes:
        parameters:
    '''

    # You need to provide host and port
    HOST = 'etcd-test'
    PORT = 2379

    # Create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)
  
    # fetch
    (await config.fetch()).unwrap()
