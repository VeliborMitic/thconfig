from thconfig.http import EtcdConfig


## error Handling fetch
async def example_7():
    '''
    Handling exception using unwrap_value function
    that returns exception's message as string and
    doesen't raise exception
    
    Instantiate EtcdConfig,
    Fetch changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False
            
    Fetch changes:
        parameters:
            self: EtcdConfig
    '''
    
    # Wrong port
    HOST = 'wrong-host'
    PORT = '1111'

    # create intance EtcdConfig by given wrong URI
    config = EtcdConfig(host = HOST, port = PORT)
 
    # fetch and unwraps_value - returns exception's message as string
    (await config.fetch()).unwrap_value()
      