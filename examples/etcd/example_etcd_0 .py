from thconfig.http import EtcdConfig

 
'''
Instantiate EtcdConfig

parameters:
    host: str,
    port: int,
    fetch: bool = True, 
    commit: bool = False
'''

# you need to provide host and port
HOST = 'etcd-test'
PORT = 2379

# create instance of EtcdConfig
config = EtcdConfig(host = HOST, port = PORT)
