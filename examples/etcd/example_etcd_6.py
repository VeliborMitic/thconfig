from thconfig.http import EtcdConfig

async def example_6():
    '''
    A standard wat to handling exceptions
    with try and except block
    
    Instantiate EtcdConfig,
    Fetch changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False
            
    Fetch changes:
        parameters:
            self: EtcdConfig
    '''
    
    # you need to provide host and port
    HOST = 'wrong-host'
    PORT = 2379

    # create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)

    # standard way to handle exception using try and except blocks
    try:
        # fetch
        (await config.fetch()).unwrap()
    except:
        pass
