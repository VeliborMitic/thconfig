from thconfig.http import EtcdConfig


## Error Handling fetch
async def example_8():
    '''
    Handling exception using unwrap_or function
    that returns custom_message as string and
    doesen't raise exception
    
    Instantiate EtcdConfig,
    Fetch changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False

    Fetch changes:
        parameters:
            self: EtcdConfig
            
    unwrap_or:
        parameters:
            custom_message: str
    '''
    
    # You need to provide host and port
    HOST = 'wrong-host'
    PORT = 2379

    # Create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)
 
    # fetch and unwraps_or - returns custom message as string
    (await config.fetch()).unwrap_or('Handling error with custom message') # Handling error with custom message
      